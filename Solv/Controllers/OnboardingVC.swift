//
//  ViewController.swift
//  Solv
//
//  Created by Naresh Muthuluri on 27/11/19.
//  Copyright © 2019 Naresh Muthuluri. All rights reserved.
//

import UIKit

class OnboardingVC: UIViewController {

    @IBOutlet weak var onboardingCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

extension OnboardingVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let onboardingCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCell", for: indexPath)
        return onboardingCell
    }
    
    
}
