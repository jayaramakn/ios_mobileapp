//
//  ServiceType.swift
//  Solv
//
//  Created by Naresh Muthuluri on 27/11/19.
//  Copyright © 2019 Naresh Muthuluri. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

enum APIServiceType {
    case socialSignIn
    

    static func service(for aServiceType: APIServiceType) -> String {
        var subPath = ""
        switch aServiceType {
        case .socialSignIn:
            subPath = ""
        }
        return BASE_URL + subPath
    }
}
