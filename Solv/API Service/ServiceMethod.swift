//
//  ServiceMethod.swift
//  Solv
//
//  Created by Naresh Muthuluri on 27/11/19.
//  Copyright © 2019 Naresh Muthuluri. All rights reserved.
//

import Foundation
import Alamofire

enum APIServiceMethod {
    case post
    case get
    case put
    case delete
}

typealias APIServiceCompletionHandler = ((_ result: Any?, _ error: Error?) ->Void)

class APIService {
    static func perform(serviceType aType: APIServiceType,
                        method: APIServiceMethod,
                        params: [String: Any]?,
                        completion: @escaping APIServiceCompletionHandler) {
        let urlPath = APIServiceType.service(for: aType)
        print(urlPath)
        
        guard let url = URL(string: urlPath) else {
            let error = NSError(domain: "apiservice.error", code: 1504, userInfo: [NSLocalizedDescriptionKey: "Request url is invalid.", NSLocalizedFailureReasonErrorKey: "Invalid url path for '\(urlPath)' service type"])
            completion(nil, error)
            return
        }
        
        switch method {
        case .post:
            sendingPostRequest(url: url, params: params, completion: completion)
        case .get:
            sendingGetRequest(url: url, params: params, completion: completion)
        case .put:
            sendingPostRequestWithFormData(url: url, params: params, completion: completion)
        default:
            break
        }
    }
    
    static func sendingPostRequest(url: URL,
                                   params: [String: Any]?,
                                   completion: @escaping APIServiceCompletionHandler) {
        let headers = ["Content-Type": "application/json"]
        print("Request headers \n \(headers)")
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result{
            case .success(_):
                completion(response.result.value, nil)
                break
            case .failure(let error):
                completion(nil, error)
                break
            }
        }
    }
    
    static func sendingGetRequest(url: URL,
                                  params: [String: Any]?,
                                  completion: @escaping APIServiceCompletionHandler) {
        let headers = ["Content-Type": "Application/json"]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result{
            case .success(_):
                completion(response.result.value, nil)
                break
            case .failure(let error):
                completion(nil, error)
                break
            }
        }
    }
    
    static func sendingPostRequestWithFormData(url: URL,
                                               params: [String: Any]?,
                                               completion: @escaping APIServiceCompletionHandler) {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result{
            case .success(_):
                completion(response.result.value, nil)
                break
            case .failure(let error):
                completion(nil, error)
                break
            }
        }
    }
    
}
